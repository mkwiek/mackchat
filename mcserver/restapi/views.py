from restapi.models import Chatter
from rest_framework import viewsets
from restapi.serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
import random


class ChatterViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Chatter.objects.all()
    serializer_class = UserSerializer

class ChatterPair(APIView):
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        usernames = [chatter.name for chatter in Chatter.objects.filter(chat='')]
        if len(usernames) < 2:
            return Response([])
        else:
            return Response(random.sample(set(usernames), 2))