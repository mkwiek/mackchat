from django.db import models

# Create your models here.
class Chatter(models.Model):
    name = models.TextField()
    chat = models.TextField(default=None, blank=True, null=True)
