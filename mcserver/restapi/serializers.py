from restapi.models import Chatter
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Chatter
        fields = ('name','chat','url')
        id = serializers.Field()
