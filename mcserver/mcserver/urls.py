from django.conf.urls import patterns, url, include
from rest_framework import routers
from restapi import views

router = routers.DefaultRouter()
router.register(r'chatters', views.ChatterViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^chatPair', views.ChatterPair.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)