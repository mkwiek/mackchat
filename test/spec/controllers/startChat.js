'use strict';

describe('Controller: StartchatCtrl', function () {

  // load the controller's module
  beforeEach(module('mackchatApp'));

  var StartchatCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StartchatCtrl = $controller('StartchatCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
