var API_KEY = 'default';
var FIREBASE_URL = 'https://macktest.firebaseio.com' + '/' + API_KEY;
var REST_URL = 'http://127.0.0.1:8000';

var Firebase = require('firebase');
var RestClient = require('node-rest-client').Client;

var client = new RestClient();

var usersRef = new Firebase(FIREBASE_URL + '/users');

var chatterUrls = {};

function onUserAdded(snapshot) {
	var name = snapshot.name();
	var args = {
        data:{name:name, chat:snapshot.val().chat},
        headers : {"content-type": "application/json"},
    };
    console.log(args);

	client.post(REST_URL + "/chatters/", args, function(data, response) {
		console.log("Added user");
		chatterUrls[name] = data.url;
	});
}

function onUserRemoved(snapshot) {
	client.delete(chatterUrls[snapshot.name()], {}, function(data,response){
	});
}

usersRef.on('child_added', onUserAdded);
usersRef.on('child_removed', onUserRemoved);


function callWithNextChatCouple(callback) {
	client.get(REST_URL + "/chatPair/", function(data,response) {
		callback(data);
	});
}

function matchChat() {
	callWithNextChatCouple(function(names) {
		if(names.length == 2) {
        var chatsRef = new Firebase(FIREBASE_URL + '/chats/');
        var chatName = chatsRef.push({initiator: 'Someone'}).name();

		var userRefs = names.map(function(name) {
			return new Firebase(FIREBASE_URL + '/users/' + name);
		});

		userRefs[0].child('partner').set(userRefs[1].name());
		userRefs[1].child('partner').set(userRefs[0].name());

		userRefs.forEach(function(reference, index) {
			reference.child('chat').set(chatName);

			var newVal = {name : names[index], chat : chatName};
			client.put(chatterUrls[names[index]], {data : newVal, headers : {"content-type": "application/json"}});
		});
		}
	});
}

setInterval(matchChat, 5000);