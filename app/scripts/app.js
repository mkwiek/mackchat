'use strict';

var app = angular.module('mackchatApp', [
  'ngCookies',
  'ngRoute',
  'ngAnimate',
  'firebase'
]);

app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/startchat'
      })
      .when('/startchat', {
        templateUrl: 'views/startchat.html',
        controller: 'StartChatCtrl'
      })
      .when('/chat', {
        templateUrl: 'views/chat.html',
        controller: 'ChatCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app.factory('firebaseBaseUrl', function() {
  return function() {
    return 'https://macktest.firebaseio.com';
  }
});