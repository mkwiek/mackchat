'use strict';

app.controller('StartChatCtrl', ['$scope', '$rootScope', '$location', '$routeParams', 'angularFire', 'firebaseBaseUrl',
 function ($scope, $rootScope, $location, $routeParams, angularFire, firebaseBaseUrl) {
  $scope.interest = false;
  $scope.searching = false;

  if($routeParams.key) {
    $rootScope.firebaseUrl = firebaseBaseUrl() + '/' + $routeParams.key;
  }else {
    $rootScope.firebaseUrl = firebaseBaseUrl() + '/default';
  }
  var firebaseUrl = $rootScope.firebaseUrl;

  var ref = new Firebase(firebaseUrl + '/users');

  if(typeof $rootScope.user === 'undefined'){
    $scope.user = {name : '', partner : '', isHost : false, chat : ''};
  }else {
    $scope.user = $rootScope.user;
  }

  ref.once('value', function(usersSnapshot){

    $rootScope.chat = null;
    $rootScope.partnerRef = null;
    $rootScope.selfRef = ref.push($scope.user);

    var selfRef = $scope.selfRef;
    selfRef.setPriority(usersSnapshot.numChildren() + 1);
    selfRef.onDisconnect().remove();

    selfRef.on('value', function(snapshot) {
      if(snapshot.val()) {
        var chatName = snapshot.val().chat;
        if(chatName.length > 0) {
          $rootScope.chatName = chatName;
          $rootScope.partnerRef = new Firebase(firebaseUrl + '/users/' + snapshot.val().partner);
          $rootScope.user = $scope.user;
          $location.path('/chat');
        }
      }
    });

    angularFire(selfRef, $scope, 'user');

    $scope.start = function() {
      if($scope.user.name.length === 0) {
        alert('Please insert your name');
        return false;
      }else if(!$scope.searching){
        $scope.user.isHost = true;
        $scope.searching = true;

        var onceMore = function(ref, priority, callback) {
          ref.startAt(priority + 1).once('child_added', callback);
        } 

        var checkChild = function(snapshot){
          if($scope.user.partner.length > 0) {
          }else if($scope.user.partner.length === 0 
            && snapshot.val().partner.length === 0 
            && snapshot.name() !== selfRef.name()){

            var singleRef = new Firebase(firebaseUrl + '/users/'+snapshot.name());

            singleRef.transaction(function(value){
              if(value.partner.length === 0 && !value.isHost) {
                value.partner = selfRef.name();
              }
              return value;
            }, function(error, commited, snap){
              if(commited) {
                selfRef.child('partner').set(snapshot.name());
                $scope.user.partner = snapshot.name();
                var chatsRef = new Firebase(firebaseUrl + '/chats/');
                var chatName = chatsRef.push({initiator: $scope.user.name}).name();
                selfRef.child('chat').set(chatName);
                singleRef.child('chat').set(chatName);
              }else {
                onceMore(ref, snapshot.getPriority(), checkChild);
                // ref.startAt(snapshot.getPriority() + 1).once('child_added', checkChild);
              }
            });
          }else {
            onceMore(ref, snapshot.getPriority(), checkChild);
            // ref.startAt(snapshot.getPriority() + 1).once('child_added', checkChild);
          }
        }

        
        ref.once('child_added', checkChild);
      }
    };
  });
}]);

