'use strict';

app.controller('ChatCtrl', ['$scope', '$rootScope', '$location', '$timeout', '$interval', 'angularFire', 
  function ($scope, $rootScope, $location, $timeout, $interval, angularFire) {
    if(typeof $rootScope.chatName  === 'undefined' || typeof $rootScope.partnerRef === 'undefined') {
      $location.path('/startchat');
    }

    var firebaseUrl = $rootScope.firebaseUrl;

    var chatRef = new Firebase(firebaseUrl + '/chats/' + $rootScope.chatName);

    $scope.beginChat = function(){
      $scope.approved = true;

      angularFire(chatRef.limit(10), $scope, 'messages');

      $scope.partnerOnline = true;

      $rootScope.partnerRef.on('value', function(snapshot) {
        if(snapshot.val() === null) {
          $scope.partnerOnline = false;
          $rootScope.user.partner = '';
          $rootScope.user.isHost = false;
          $rootScope.selfRef.remove();
          $scope.$apply();
        }
      });

      $scope.addMessage = function() {
        if($scope.messages === null) {
          $scope.messages = {};
        }
        $scope.messages[chatRef.push().name()] = {
          from: $scope.user.name,
          content: $scope.message
        };
        $scope.message = '';
      };
    };

    if($scope.user.isHost) {
      $scope.approved = true;
      $scope.beginChat();
    }else {
      var chatInitiatorRef = new Firebase(firebaseUrl + '/chats/' + $rootScope.chatName + '/initiator');

      chatInitiatorRef.once('value', function(initiatorSnapshot) {
        $scope.partnerName = initiatorSnapshot.val();
        $scope.approved = false;
        $scope.seconds = 15;
        $timeout(function() {
          if(!$scope.approved){
            window.location.reload(true);
          }
        }, 1000 * $scope.seconds);

        $interval(function(){
          if($scope.seconds > 0) {
            $scope.seconds--;
          }
        }, 1000);
      });
    }
  }]);